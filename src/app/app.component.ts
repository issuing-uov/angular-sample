import { Component } from '@angular/core';

export interface User {
  firstName: string;
  lastName: string;
  company: string;
  age: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'sample';

  public users: Array<User> = [
    { firstName: 'Max', lastName: 'Musterman', company: 'Amazon', age: 23 },
    { firstName: 'Max', lastName: 'Musterman', company: 'Amazon', age: 23 },
    { firstName: 'Max', lastName: 'Musterman', company: 'Amazon', age: 23 },
    { firstName: 'Max', lastName: 'Musterman', company: 'Amazon', age: 23 },
    { firstName: 'Max', lastName: 'Musterman', company: 'Amazon', age: 23 },
    { firstName: 'Max', lastName: 'Musterman', company: 'Amazon', age: 23 },
    { firstName: 'Max', lastName: 'Musterman', company: 'Amazon', age: 23 },
    { firstName: 'Max', lastName: 'Musterman', company: 'Amazon', age: 23 },
  ];
}
